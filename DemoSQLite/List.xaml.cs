﻿using DemoSQLite.Entity;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DemoSQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class List : Page
    {
        public List()
        {
            this.InitializeComponent();
            Loaded += List_Loaded;
        }
        private void List_Loaded(object sender, RoutedEventArgs e)
        {
            List<Transaction> listtransection = new List<Transaction>();
            SQLiteConnection cnn = new SQLiteConnection("transaction.db");
            using (var stt = cnn.Prepare("select * from PersonalTransaction"))
            {
                while (stt.Step()==SQLiteResult.ROW)
                {
                    Transaction transaction = new Transaction()
                    {
                        name = (string)stt["Name"],
                        description = (string)stt["Description"],
                        amount = (double)stt["Amount"],
                        created_at = (string)stt["CreatedAt"],
                        category = Convert.ToInt32(stt["Category"]),
                       

                    };
                    listtransection.Add(transaction);
                }
            }
            ListTransaction.ItemsSource = listtransection;

        }
    }
}
