﻿using DemoSQLite.Entity;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DemoSQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddTransaction : Page
    {
        public AddTransaction()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime localDate = DateTime.Now;

            Transaction transaction = new Transaction()
            {
                name = name.Text,
                description = description.Text,
                amount = Convert.ToDouble(amount.Text),
                created_at = localDate.ToString("dd-mm-yyyy"),
                category = Convert.ToInt32(category.Text),
            };
            InsertToSqLite(transaction);


        }
        private void InsertToSqLite(Transaction transaction)
        {
            SQLiteConnection cnn = new SQLiteConnection("transaction.db");
           ISQLiteStatement stt = cnn.Prepare("insert into PersonalTransaction (Name,Description,Amount,CreatedAt,Category) values (?,?,?,?,?)");
            stt.Bind(1,transaction.name);
            stt.Bind(2,transaction.description);
            stt.Bind(3,transaction.amount);
            stt.Bind(4,transaction.created_at);
            stt.Bind(5,transaction.category);
            stt.Step();
        }
    }
}
