﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DemoSQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Loaded += MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            CreateTables();

        }
        public static void CreateTables()
        {
            SQLiteConnection cnn = new SQLiteConnection("transaction.db");
     
            string sql = @"CREATE TABLE IF NOT EXISTS
                          PersonalTransaction (Id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                    Name    VARCHAR( 140 ),
                                    Description    VARCHAR( 140 ),
                                    Amount DOUBLE,
                                    CreatedAt DATE,
                                    Category INT
                                    );";
            using (var statement = cnn.Prepare(sql))
            {
                statement.Step();
            }
           
        }
        private void AddTransaction(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(typeof(AddTransaction));
        }
        private void ListTransaction(object sender, RoutedEventArgs e)
        {

            MainFrame.Navigate(typeof(List));
        }

        private void category_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            Debug.WriteLine(startweek.Date.Value.ToString("dd-mm-yyyy")) ;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame)?.Content as MainFrame;


        }
    }
}
