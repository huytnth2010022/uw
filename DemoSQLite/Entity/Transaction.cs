﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace DemoSQLite.Entity
{
    class Transaction
    {
        public string name { get; set; }
        public string description { get; set; }
        public double amount { get; set; }
        public string created_at { get; set; }
        public int category { get; set; }



    }
}
