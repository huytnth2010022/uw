﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NET_MVC.Data
{
    public class NET_MVCContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public NET_MVCContext() : base("name=DemoContextSqlServer")
        {
        }

      
        public System.Data.Entity.DbSet<NET_MVC.Models.Student> Students { get; set; }

        public System.Data.Entity.DbSet<NET_MVC.Models.Demo> Demoes { get; set; }
    }
}
