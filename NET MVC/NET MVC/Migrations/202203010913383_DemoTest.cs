namespace NET_MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoTest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Demoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        city = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Demoes");
        }
    }
}
