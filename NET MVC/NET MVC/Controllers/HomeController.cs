﻿using NET_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NET_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(PersonModel person)
        {
            /*     TempData["phone"] = 0773776942;*/
            /*return Redirect("/Home/About");*/
            int personId = person.PersonId;
            string name = person.Name;
            string gender = person.Gender;
            string city = person.City;

            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}