﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NET_MVC.Models.ViewModels
{
    public class ViewStudent
    {
        [DisplayName("Mã Tài khoản")]
        public int ID { get; set; }
        [DisplayName(" Tài khoản")]
        [Required(ErrorMessage = "Vui lòng nhập Tài khoản")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "vui lòng nhập tài khoản khoảng(5 - 20) ký tự")]
        public string UserName { get; set; }
        [DisplayName("Mật Khẩu")]
        [Required(ErrorMessage = "Vui lòng nhập Mật khẩu")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DisplayName("Nhập lại Mật Khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Vui lòng nhập lại Mật khẩu")]
        [Compare("Password", ErrorMessage = "Mật khẩu không khớp")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Bạn nhập chưa đúng định dạng (VD:htmm@gmail.com)")]

        public string Email { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        [RegularExpression(@"(84|0[3|5|7|8|9])+([0-9]{8})", ErrorMessage = "vui lòng nhập đúng định dạng")]
        [DisplayName("Nhập Số điện thoại")]
        public string PhoneNumber { set; get; }
        [Range(1, 3, ErrorMessage = "trạng thái tài khoản phải có giá trị từ 1 đến 3")]
        public int Status { set; get; }
      
    }
}