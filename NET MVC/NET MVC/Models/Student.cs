﻿using NET_MVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace NET_MVC.Models
{
    public class Student
    {
        private readonly ViewStudent viewStudent;

        public int ID { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 5)]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"(84|0[3|5|7|8|9])+([0-9]{8})")]
        public string PhoneNumber { set; get; }
        [Range(1, 3)]
        public int Status { set; get; }
        public Student()
        {

        }
 
        public Student(ViewStudent viewStudent)
        {
            this.Email = viewStudent.Email;
            this.UserName = viewStudent.UserName;
            this.Password = viewStudent.Password;
            this.PhoneNumber = viewStudent.PhoneNumber;
            this.Status = viewStudent.Status;


        }
    }
 
}