﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using AssigmentUWP.Entity;
using Newtonsoft.Json;
using System.Diagnostics;

namespace AssigmentUWP.Service
{
    class GetAccount
    {
        private async Task<Account> AccInformation(string token)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                var result = await httpClient.GetAsync($"{ConfigFile.ConfigClass.ApiDomain}{ConfigFile.ConfigClass.AccountPath}");
                var content = await result.Content.ReadAsStringAsync();

                Debug.WriteLine(content);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Account account = JsonConvert.DeserializeObject<Account>(content);
                    return account;
                }
                else
                {
                    return null;
                }

            }
        }
        public async Task<Account> GetLoggedInAccount()
        {
            Account acc;
            LoginClass loginClass = new LoginClass();
            Credetial credetial = await loginClass.LoadToken();
            if(credetial == null)
            {
                return null;
                
            }
            else
            {
                acc = await AccInformation(credetial.access_token);
                return acc;
            }
         
        }
    }
}
