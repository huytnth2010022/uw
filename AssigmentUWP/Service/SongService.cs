﻿using AssigmentUWP.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentUWP.Service
{
    class SongService
    {
        public async Task<List<Song>> GetListSong()
        {
            using (HttpClient httpClient = new HttpClient())
            {

                var result = await httpClient.GetAsync($"{ConfigFile.ConfigClass.ApiDomain}{ConfigFile.ConfigClass.ListSong}");
                var content = await result.Content.ReadAsStringAsync();
                List<Song> Songs = new List<Song>();

                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                   
                    return JsonConvert.DeserializeObject<List<Song>>(content);
                }
                else
                {
                    return Songs;
                }

            }
        }
    }
}
