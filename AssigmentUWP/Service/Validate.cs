﻿using AssigmentUWP.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AssigmentUWP.Service
{
    class Validate
    {
        Dictionary<string, string> error = new Dictionary<string, string>();
       
        public Dictionary<string, string> validateInput(Account acc)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            var regexEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            var regexPhone = new Regex(@"(^84|0[3|5|7|8|9])+([0-9]{8})\b");
            Regex regexUrl = new Regex(@"^(http|ftp|https|www)://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?$", RegexOptions.IgnoreCase);
            //firstname
            if (acc.firstName == "")
            {
                error.Add("firstname", "không được để trống");

            }
            else
            {
               
                if (!regexItem.IsMatch(acc.firstName))
                {
                    error.Add("firstname", "không chứa ký tự đặc biệt");
                }
            }
            //lastname
            if (acc.lastName == "")
            {
                error.Add("lastname", "không được để trống");

            }
            else
            {
                if (!regexItem.IsMatch(acc.lastName))
                {
                    error.Add("lastname", "không chứa ký tự đặc biệt");
                }
            }
            //password
            if (acc.password == "")
            {
                error.Add("password", "không được để trống");

            }
            else
            {

                if (!regexItem.IsMatch(acc.password))
                {
                    error.Add("password", "không chứa ký tự đặc biệt");
                }
                else { 
                if(acc.password.Length > 16)
                    {
                        error.Add("password", "Mật khẩu quá dài");
                    }
                }
            }
            //email
            if (acc.email == "")
            {
                error.Add("email", "không được để trống");

            }
            else
            {

                if (!regexEmail.IsMatch(acc.email))
                {
                    error.Add("email", "Không đúng định dạng");
                }
              
            }
            //address
            if (acc.address == "")
            {
                error.Add("address", "không được để trống");
            }
            //phone
            if (acc.phone == "")
            {
                error.Add("phone", "không được để trống");

            }
            else
            {
                if (!regexPhone.IsMatch(acc.phone))
                {
                    error.Add("phone", "Không đúng định dạng");
                }
            }
            //avatar
            if (acc.avatar == "")
            {
                error.Add("avatar", "không được để trống");

            }
            else
            {
                if (!regexUrl.IsMatch(acc.avatar))
                {
                    error.Add("avatar", "Không đúng định dạng");
                }
            }
            //birthday
            if (acc.birthday == null)
            {
                error.Add("birthday", "không được để trống");
            }
            else
            {  
               var date = DateTime.Parse(acc.birthday);
               DateTime localDate = DateTime.Now;
     
              
                int res = DateTime.Compare(date, localDate.AddYears(-12));
                if(res > 0)
                {
                    error.Add("birthday", "Học bài đi cháu");
                }
          
            }
            return error;
        }
  
    }
}
