﻿using AssigmentUWP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Diagnostics;
using Windows.Storage;

namespace AssigmentUWP.Service
{
    class LoginClass
    {
        private const string TokenFileName = "token.txt";
        public async  Task Login(LoginEntity loginEntity) {
            var jsonString = JsonConvert.SerializeObject(loginEntity);
            using (HttpClient httpClient = new HttpClient()) {
                HttpContent contentToSend = new StringContent(jsonString, Encoding.UTF8, ConfigFile.ConfigClass.MediaType);
                var result = await httpClient.PostAsync($"{ConfigFile.ConfigClass.ApiDomain}{ConfigFile.ConfigClass.Authentication}", contentToSend);
                var content = await result.Content.ReadAsStringAsync();

                Debug.WriteLine(content);
                if(result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //good code
                    SaveToken(content);
                    
                }
                else
                {
                    Debug.WriteLine("Error500");
                 
                }

            }
        }

     
        private async void SaveToken(string content)
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile storageFile = await storageFolder.CreateFileAsync(TokenFileName,Windows.Storage.CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(storageFile, content);
        }
    
        public async Task<Credetial> LoadToken()
        {
            try
            {
                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                StorageFile storageFile = await storageFolder.GetFileAsync(TokenFileName);

                string TokenContent = await FileIO.ReadTextAsync(storageFile);
                Credetial credetial = JsonConvert.DeserializeObject<Credetial>(TokenContent);
                return credetial;
            }
            catch (Exception e)
            {
                return null;
            }

        }
    }
   
}
