﻿using AssigmentUWP.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentUWP.Service
{
    class RegisterClass
    {

        public async void RegisterApi(Account acc)
        {
            var jsonString = JsonConvert.SerializeObject(acc);
            HttpClient httpClient = new HttpClient();
            HttpContent contentToSend = new StringContent(jsonString, Encoding.UTF8, ConfigFile.ConfigClass.MediaType);
            var result = await httpClient.PostAsync($"{ConfigFile.ConfigClass.ApiDomain}{ConfigFile.ConfigClass.AccountPath}", contentToSend);
            if (result.StatusCode == System.Net.HttpStatusCode.Created)
            {
                // good case.
                var content = await result.Content.ReadAsStringAsync();
                Account returnAccount = JsonConvert.DeserializeObject<Account>(content);
                Debug.WriteLine(returnAccount.id);
                Debug.WriteLine(returnAccount.firstName);
            }
            else
            {
                Debug.WriteLine("error500");
            }

        }
       
    }
}

