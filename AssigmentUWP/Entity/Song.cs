﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace AssigmentUWP.Entity
{
    class Song
    {
        public int id { set; get; }
        public int account_id { set; get; }
        public string name { set; get; }
        public string description { set; get; }
        public string singer { set; get; }
        public string author { set; get; }
        public string link { set; get; }
 
        public string thumbnail { set; get; }
        public string message { set; get; }
        public string created_at { set; get; }
        public string updated_at { set; get; }
        public int status { set; get; }

        public void Createdat(CalendarDatePicker birthday)
        {
            this.created_at = birthday.Date.Value.ToString("dd-MM-yyyy");

        }

    }
}
