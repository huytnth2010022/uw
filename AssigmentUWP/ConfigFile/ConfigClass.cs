﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentUWP.ConfigFile
{
    class ConfigClass
    {
        public static string ApiDomain = "https://music-i-like.herokuapp.com";
        public static string AccountPath = "/api/v1/accounts";
        public static string ListSong = "/api/v1/songs";
        public static string Authentication = "/api/v1/accounts/authentication";
        public static string MediaType = "application/json";

    }
}
