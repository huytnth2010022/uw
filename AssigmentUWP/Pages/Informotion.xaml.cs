﻿using AssigmentUWP.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AssigmentUWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Informotion : Page
    {
        public Informotion()
        {
            this.InitializeComponent();
            Loaded += Informotion_Loaded;
            List<Account> accounts = new List<Account>();
            Account account = App.Checklogin;
            accounts.Add(account);
            MyView.ItemsSource = accounts;
        }

        private void Informotion_Loaded(object sender, RoutedEventArgs e)
        {
            LoadImage();
        }

        async void LoadImage()
        {
            Account account = App.Checklogin;
            string url = account.avatar;

            var rass = RandomAccessStreamReference.CreateFromUri(new Uri(url));
            using (IRandomAccessStream stream = await rass.OpenReadAsync())
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.SetSource(stream);
                ImageStudent.Source = bitmapImage;
            }
        }

    }
}
