﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AssigmentUWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NavBar : Page
    {

        private readonly List<(string Tag, Type Page)> _pages = new List<(string Tag, Type Page)>
{
  

};
        private object register;

        public NavBar()
        {
            this.InitializeComponent();
            Loaded += NavBar_Loaded;
        }

       private void NavBar_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.Checklogin !=  null)
            {
                navz.MenuItems.Add(new NavigationViewItemSeparator());
                 NavigationViewItem navigationViewItem  = new NavigationViewItem();
                navigationViewItem.Content = "My Information";
                navigationViewItem.Tag = "information";
                navigationViewItem.Icon = new SymbolIcon((Symbol)0xE136);
                navz.MenuItems.Add(navigationViewItem);
                _pages.Add(("information", typeof(Pages.Informotion)));

                navz.MenuItems.Add(new NavigationViewItemSeparator());
                NavigationViewItem Listsong = new NavigationViewItem();
                Listsong.Content = "Listsong";
                Listsong.Tag = "Listsong";
                Listsong.Icon = new SymbolIcon((Symbol)0xE8D6);
                navz.MenuItems.Add(Listsong);
                _pages.Add(("Listsong", typeof(Pages.ListSong)));

            }
            else
            {
                navz.MenuItems.Add(new NavigationViewItemSeparator());
                NavigationViewItem RegisterView = new NavigationViewItem();
                RegisterView.Content = "Register";
                RegisterView.Tag = "register";
                RegisterView.Icon = new SymbolIcon((Symbol)0xEA8C);

                navz.MenuItems.Add(RegisterView);
                _pages.Add(("register", typeof(Pages.Register)));
              
                NavigationViewItem LoginView = new NavigationViewItem();
                LoginView.Content = "Login";
                LoginView.Tag = "login";
                LoginView.Icon = new SymbolIcon((Symbol)0xE726);

                navz.MenuItems.Add(LoginView);
                _pages.Add(("login", typeof(Pages.Login)));

            }

        }

        private void navz_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked == true)
            {
                MainContent.Navigate(typeof(Setting.Setting));
            }
            else
            {
                var selectedNav = sender.SelectedItem as NavigationViewItem;
                var item = _pages.FirstOrDefault(p => p.Tag.Equals(selectedNav.Tag));
                MainContent.Navigate(item.Page);

            }
        }

        private void navz_Loaded(object sender, RoutedEventArgs e)
        {
 
        }
    }
}
