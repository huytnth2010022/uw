﻿using AssigmentUWP.Entity;
using AssigmentUWP.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AssigmentUWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListSong : Page
    {
       private SongService songService;
        public ListSong()
        {
            this.InitializeComponent();
            songService = new SongService();
            Loaded += ListSong_Loaded;
        }

        private async void ListSong_Loaded(object sender, RoutedEventArgs e)
        {
            List<Song> Songs = await this.songService.GetListSong();
            ObservableCollection<Song> ob = new ObservableCollection<Song>(Songs);
            listView.ItemsSource = ob;
        


        }

      
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            mediaPlayer.MediaPlayer.Pause();
            base.OnNavigatedFrom(e);
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var currenSong = listView.SelectedItem as Song;
            Debug.WriteLine(currenSong.link);
            Debug.WriteLine(currenSong.name);
            mediaPlayer.MediaPlayer.Source = MediaSource.CreateFromUri(new Uri(currenSong.link));
            mediaPlayer.MediaPlayer.Play();
        }
    }
}
