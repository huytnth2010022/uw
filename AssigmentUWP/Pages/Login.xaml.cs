﻿using AssigmentUWP.Entity;
using AssigmentUWP.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AssigmentUWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        public Login()
        {
            this.InitializeComponent();
            Loaded += Login_Loaded;
        }

        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            erroremail.Text = "";
            errorpassword.Text = "";
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            LoginEntity login = new LoginEntity();
            login.email = email.Text;
            login.password = password.Password;
            erroremail.Text = "";
            errorpassword.Text = "";
            if (login.email == "")
            {
                erroremail.Text = "Không được để trống";
                return;

            }
            else
            {
                if (login.password == "")
                {
                    errorpassword.Text = "không được để trống";
                    return;
                }
                else
                {
                    LoginClass loginClass = new LoginClass();
              await loginClass.Login(login);
                    Restart();
   






                }
            }
           

        }
   
        private async void Restart()
        {
            var result = await CoreApplication.RequestRestartAsync("Application Restart Programmatically ");

            if (result == AppRestartFailureReason.NotInForeground ||
                result == AppRestartFailureReason.RestartPending ||
                result == AppRestartFailureReason.Other)
            {
                var msgBox = new MessageDialog("Restart Failed", result.ToString());
                await msgBox.ShowAsync();
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Pages.Register));
        }
    }
}
