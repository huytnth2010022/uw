﻿using AssigmentUWP.Entity;
using AssigmentUWP.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AssigmentUWP.Pages
{
 
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    public sealed partial class Register : Page
    {
        public  int radioChose = 0;
        public Register()
        {
            this.InitializeComponent();
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {

        }
        private void Clean_Text_Error()
        {
            errorfname.Text = "";
            errorlname.Text = "";
            erroravatar.Text = "";
            erroremail.Text = "";
            errorphone.Text = "";
            errorpassword.Text = "";
            erroraddress.Text = "";
            errorbirthday.Text = "";
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
            Account acc = new Account();
            acc.firstName = firstname.Text;
                      acc.lastName = lastname.Text;
                     acc.phone = phone.Text;
                        acc.email = email.Text;
                        acc.password = password.Password;
                     acc.address = address.Text;
               acc.avatar = avatar.Text;
            acc.gender = radioChose;
            if (birthday != null)
            {
                Debug.WriteLine("hello");
                acc.Birthday(birthday);
            }
            else
            {
                errorbirthday.Text = "không được để trống";
            }
            Clean_Text_Error();
            Validate validate = new Validate();
          var error=  validate.validateInput(acc);
            var keys = error.Keys;
      
            foreach (var key in keys)
            {
                switch (key.ToString())
                {
                    case "firstname":
                        errorfname.Text = error[key].ToString();
                        break;
                    case "lastname":
                        errorlname.Text = error[key].ToString();
                        break;
                    case "email":
                        erroremail.Text = error[key].ToString();
                        break;
                    case "password":
                        errorpassword.Text = error[key].ToString();
                        break;
                    case "address":
                        erroraddress.Text = error[key].ToString();
                        break;
                    case "phone":
                        errorphone.Text = error[key].ToString();
                        break;
                    case "avatar":
                        erroravatar.Text = error[key].ToString();
                        break;
                    case "birthday":
                        errorbirthday.Text = error[key].ToString();
                        break;

                }
                
            }
            if(error.Count > 0)
            {
                return;
            }
            else
            {
                RegisterClass registerClass = new RegisterClass();
                registerClass.RegisterApi(acc);
            }
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            var radioButton = (RadioButton)sender;
            switch (radioButton.Tag) {
                case 1:
                    radioChose = 1;
                    break;
                case 2:
                    radioChose = 2;
                    break;
            }
            
        }

        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {

        }
    }
}
